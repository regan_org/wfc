package com.we.wfc.security.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.we.wfc.security.entity.OauthRole;
import com.we.wfc.security.entity.OauthUser;
import com.we.wfc.security.entity.Role;
import com.we.wfc.security.entity.User;
import com.we.wfc.security.mapper.RoleDao;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 加载用户特定数据的核心接口（Core interface which loads user-specific data.）
 *
 * @author zhangby
 * @date 2019-05-14 09:49
 */
@Service
@AllArgsConstructor
public class UserServiceDetail implements UserDetailsService {

    private final RoleDao roleDao;

    /**
     * 根据用户名查询用户
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        OauthUser oauthUser = new OauthUser();
//        查询登录用户
        User user = new User().selectOne(new LambdaQueryWrapper<User>().eq(User::getLoginName, username));
        Optional.of(user).ifPresent(us ->{
            //查询角色
            List<Role> roleByUser = roleDao.getRoleByUser(user.getId());
            //数据类型转换
            List<OauthRole> oauthRoles = roleByUser.stream()
                    .collect(ArrayList::new, (li, role) -> {
                        OauthRole oauthRole = new OauthRole(role.getId(),
                                Optional.ofNullable(role).map(Role::getEnname).orElse(null));
                        li.add(oauthRole);
                    }, List::addAll);
            //数据结果集封装
            oauthUser.setId(us.getId());
            oauthUser.setPassword(user.getPassword());
            oauthUser.setUsername(user.getLoginName());
            oauthUser.setOauthRoles(oauthRoles);
        });
        //判断登录入口
        return oauthUser;
    }
}
