package com.we.wfc.security.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@Data
@Accessors(chain = true)
@TableName("sys_role")
public class Role extends Model<Role> {

private static final long serialVersionUID=1L;

    private String id;

    private String officeId;

    private String name;

    private String enname;

    private String roleType;

    private String dataScope;

    private String isSys;

    private String useable;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    private String remarks;

    @TableLogic
    private String delFlag;

}
