package com.we.wfc.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.security.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@Repository
public interface RoleDao extends BaseMapper<Role> {

    /**
     * query user info by userId
     *
     * @param userId
     * @return List
     */
    List<Role> getRoleByUser(@Param("userId") String userId);
}
