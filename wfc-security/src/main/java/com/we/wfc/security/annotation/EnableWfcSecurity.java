package com.we.wfc.security.annotation;

import com.we.wfc.security.WfcSecurityImportSelector;
import com.we.wfc.security.oauth.WeResourceConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: 认证引入注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/10 8:27 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({WfcSecurityImportSelector.class, WeResourceConfiguration.class})
public @interface EnableWfcSecurity {

}
