package com.we.wfc.security.constants;


import com.we.wfc.common.constants.BaseConstants;

/**
 * @Description: 认证、授权用常量类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/30 4:32 下午
 */
public class GrantConstants extends BaseConstants {

    public static final String FILTER_ALLOW_PATH = ".*swagger.*,.*v2.*,/actuator.*,/assets.*,/instances.*,.*hystrix.*,.*webjars.*,/oauth.*,/doc.html,/n.**";

    public static final String FILTER_NOT_ALLOW_PATH = "/g,/d";
}
