package com.we.wfc.file.annotation;

import com.we.wfc.file.WfcFileImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description: file引入注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/10 8:27 上午
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(WfcFileImportSelector.class)
public @interface EnableWfcFile {
}
