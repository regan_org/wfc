package com.we.wfc.file.entity;

import com.we.wfc.common.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 4:17 下午
 */
@Getter
@Setter
@AllArgsConstructor
public class Files extends BaseEntity {

    /**
     * 类型：0 图片
     */
    public static final String TYPE_IMAGE = "0";
    /**
     * 类型：1 文件
     */
    public static final String TYPE_FILE = "1";
    /**
     * 类型：2 音视频
     */
    public static final String TYPE_VIDEO = "2";

    /**
     * 源数据后缀名(本地存储用)
     */
    public static final String META_DATA_SUFFIX = ".mtda";
}
