package com.we.wfc.file.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 返回文件VO
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 6:18 下午
 */
@Getter
@Setter
public class FileVo {

    @ApiModelProperty(value = "上传的文件名 as:rBH9v1351O-AITu2AASzBqvjxgs010.jpg")
    private String fileName;
    @ApiModelProperty(value = "路径 as:group1/M00/00/00/rBH9v1351O-AITu2AASzBqvjxgs010.jpg")
    private String path;
    @ApiModelProperty(value = "文件真实名 as:WechatIMG5.jpeg")
    private String oriFileName;
}
