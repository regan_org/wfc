package com.we.wfc.file.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.we.wfc.common.base.BaseEntity;
import com.we.wfc.common.serialize.OffsetDateTimeDeserializer;
import com.we.wfc.common.serialize.OffsetDateTimeSerializer;
import com.we.wfc.file.vo.FileVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.OffsetDateTime;

/**
 * <p>
 * 系统文件表
 * </p>
 *
 * @author WeCreater
 * @since 2019-12-20
 */
@Entity(name = "we_file")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "WeFile对象", description = "系统文件表")
public class WeFile extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "weid")
    @GenericGenerator(name = "weid", strategy = "uuid")
    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    private String id;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @ApiModelProperty(hidden = true)
    public OffsetDateTime createDate;

    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    public String createBy;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @ApiModelProperty(hidden = true)
    public OffsetDateTime updateDate;

    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    public String updateBy;

    @ApiModelProperty(value = "文件名,非空")
    @Column(nullable = false, length = 50)
    @TableField("file_name")
    private String fileName;

    @ApiModelProperty(value = "文件存储路径,非空")
    @Column(nullable = false, length = 255)
    @TableField("file_url")
    private String fileUrl;

    private byte delFlag;

    public WeFile(FileVo obj) {
        this.fileName = obj.getOriFileName();
        this.fileUrl = obj.getPath();
    }

    public WeFile() {
    }
}
