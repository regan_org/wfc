package com.we.wfc.file.controller;

import com.we.wfc.common.constants.BaseConstants;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.file.service.FastDfsClientService;
import com.we.wfc.file.vo.UploadVo;
import com.we.wfc.user.util.UserUtil;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 文件相关接口
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 2:43 下午
 */
@Api(tags = "文件相关接口")
@RestController
@RequestMapping(value = "/grant/file")
@AllArgsConstructor
public class FilesController {

    private final FastDfsClientService fastDfsClientService;

    @PostMapping(value = "/uploadImg")
    @ApiOperation(value = "[文件]上传图片", notes = "上传文件,需要声明Content-Type=multipart/form-data")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sourNameFlag", value = "是否显示源文件名称", dataType = "boolean",
                    allowableValues = "0:否 1:是")
    })
    public ResultPoJo<UploadVo> uploadImg(MultipartHttpServletRequest request,
                                          boolean sourNameFlag) throws Exception {
        String createBy = UserUtil.getUser().getId();
        return fastDfsClientService.uploadFile(request, BaseConstants.TYPE_IMAGE,
                sourNameFlag, createBy);
    }

    @ApiOperation(value = "[文件]上传文件", notes = "上传文件,需要声明 Content-Type=multipart/form-data")
    @PostMapping(value = "/uploadFile")
    @ApiImplicitParam(name = "sourNameFlag", value = "是否显示源文件名", dataType = "boolean",
            allowableValues = "0:否 1:是")
    public ResultPoJo<UploadVo> uploadFile(MultipartHttpServletRequest request,
                                           boolean sourNameFlag) throws Exception {
        String createBy = UserUtil.getUser().getId();
        return fastDfsClientService.uploadFile(request, BaseConstants.TYPE_FILE,
                sourNameFlag, null, createBy);
    }

    @ApiOperation(value = "[文件]上传音视频", notes = "上传文件,需要声明 Content-Type=multipart/form-data")
    @PostMapping(value = "/uploadAud")
    @ApiImplicitParam(name = "sourNameFlag", value = "是否显示源文件名", dataType = "boolean",
            allowableValues = "0:否 1:是")
    public ResultPoJo<UploadVo> uploadAud(MultipartHttpServletRequest request,
                                          boolean sourNameFlag) throws Exception {
        String createBy = UserUtil.getUser().getId();
        return fastDfsClientService.uploadFile(request, BaseConstants.TYPE_VIDEO,
                sourNameFlag, null, createBy);
    }

    @ApiOperation(value = "[文件]删除文件")
    @PostMapping(value = "/delFile")
    @ApiParam(name = "filePath", value = "文件路径")
    public ResultPoJo<ReturnCode> delFile(@RequestBody String filePath) {
        return fastDfsClientService.delFile(filePath);
    }

    @ApiOperation(value = "[文件]下载网络文件到本地文件服务器并返回本地服务器地址",
            notes = "下载网络文件到本地文件服务器并返回本地服务器地址")
    @RequestMapping(value = "/snwFile", method = RequestMethod.POST)
    public ResultPoJo<String> saveNetWorkFile(@PathVariable String fileNetWorkPath,
                                              HttpServletResponse response) throws Exception {
        return fastDfsClientService.saveNetWorkFile(fileNetWorkPath);
    }
}
