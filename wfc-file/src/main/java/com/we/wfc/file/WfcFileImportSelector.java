package com.we.wfc.file;

import com.we.wfc.common.annotation.CORS;
import com.we.wfc.security.annotation.EnableResourceServer;
import com.we.wfc.security.annotation.EnableWfcSecurity;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@CORS
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.file"})
@EnableJpaRepositories({"com.we.wfc.file.repository"})
@EntityScan({"com.we.wfc.file.entity", "com.we.wfc.security.entity"})
@EnableResourceServer
public class WfcFileImportSelector {

}
