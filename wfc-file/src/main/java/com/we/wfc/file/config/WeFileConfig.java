package com.we.wfc.file.config;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 * @Description: We上传服务配置类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 10:40 上午
 */
@Configuration
//拥有FastDFS客户端
@Import(FdfsClientConfig.class)
// 解决jmx重复注册bean的问题
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
public class WeFileConfig {
}
