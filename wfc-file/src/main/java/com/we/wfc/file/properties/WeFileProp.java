package com.we.wfc.file.properties;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Check;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;
import java.util.regex.Pattern;

/**
 * @Description: We File服务配置
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/10 10:48 上午
 */
@Getter
@Setter
@ConfigurationProperties("we.upload")
public class WeFileProp {

    /**
     * 上传文件上限(单位：byte) 默认10M=10*1024(K)*1024(B)=10485760 bytes
     */
    private Long fileMaxSize = 10485760L;

    /**
     * 上传图片上限(单位：byte) 默认10M=10*1024(K)*1024(B)=10485760 bytes
     */
    private Long imageMaxSize = 10485760L;

    /**
     * 上传音视频上限(单位：byte) 默认100M=100*1024(K)*1024(B)=104857600 bytes
     */
    private Long videoMaxSize = 104857600L;

    /**
     * 上传文件类型(正则表达式) 默认:[xls|xlsx|xlsm|doc|docx|wps|pdf|txt|md|ppt|pptx|zip|rar]$
     */
    private String fileSuffix = "[xls|xlsx|xlsm|doc|docx|wps|pdf|txt|md|ppt|pptx|zip|rar]$";

    /**
     * 上传图片类型(正则表达式) 默认:[png|jpg|jpeg|bmp|gif]$
     */
    private String imageSuffix = "[png|jpg|jpeg|bmp|gif]$";

    /**
     * 上传音视频类型(正则表达式) 默认:[mp3|mp4|wmv|rm|rmvb|wav|lrc]$
     */
    private String videoSuffix = "[mp3|mp4|wmv|rm|rmvb|wav|lrc]$";

    /**
     * 文件格式正则
     */
    private Pattern filePatten;
    /**
     * 图片格式正则
     */
    private Pattern imagePatten;
    /**
     * 图片格式正则
     */
    private Pattern videoPatten;

    /**
     * fastdfs映射缓存时间(默认:秒 24*60*60)
     */
    private int mappingTimeout = 86400;

    /**
     * 使用数据库
     */
    private boolean useDatabase = true;

    /**
     * 本地存储
     */
    private LocalStorage localStorage = new LocalStorage();

    public Pattern getFilePatten() {
        if (null == this.filePatten) {
            this.filePatten = Pattern.compile(fileSuffix);
        }
        return this.filePatten;
    }

    public Pattern getImagePatten() {
        if (null == this.imagePatten) {
            this.imagePatten = Pattern.compile(imageSuffix);
        }
        return this.imagePatten;
    }

    public Pattern getVideoPatten() {
        if (null == this.videoPatten) {
            this.videoPatten = Pattern.compile(videoSuffix);
        }
        return this.videoPatten;
    }

    @Setter
    @Getter
    public static class LocalStorage {

        @NotNull
        private boolean enable = false;

        /**
         * 存储目录 绝对或相对目录 默认相对目录upload
         */
        private String path = "upload";

        /**
         * 静态资源访问路径
         */
        private String staticPathPattern = "/static/**";

        /**
         * 静态文件绝对目录 为空时默认使用 path属性的路径
         */
        private String resourceLocations;
    }
}
