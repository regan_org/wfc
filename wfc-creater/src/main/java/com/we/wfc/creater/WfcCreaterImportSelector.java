package com.we.wfc.creater;

import com.we.wfc.security.annotation.EnableResourceServer;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableDiscoveryClient
@EnableResourceServer
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.creater", "com.we.wfc.security"})
@EnableJpaRepositories({"com.we.wfc.creater.repository"})
@EntityScan({"com.we.wfc.common.entity", "com.we.wfc.creater.entity", "com.we.wfc.security.entity"})
public class WfcCreaterImportSelector {

}
