package com.we.wfc.creater.service;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.we.wfc.common.base.BaseException;
import com.we.wfc.common.base.BaseJpaEntity;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.creater.convert.MariaDBTypeConvert;
import com.we.wfc.creater.entity.WeCreater;
import com.we.wfc.creater.enums.BlackListTables;
import com.we.wfc.creater.repository.WeCreaterRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

/**
 * @Description: 创建者业务层
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/9 9:59 上午
 */
@Slf4j
@Service
@AllArgsConstructor
public class CreaterService {

    private final WeCreaterRepo weCreaterRepo;

    /**
     * 通过参数构建代码
     */
    public String buildCode(DataSourcePo po) {
        //启动构建者
        builder(po);

        //线程暂停三秒
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            new BaseException(ReturnCode.FAIL, e);
        }
        //询问是否已经生成成功
        String projectPath = System.getProperty("user.dir") + "/other/com";
        File file = new File(projectPath);
        if (file.exists()) {
            return "生成成功";
        }
        return "生成失败";
    }

    /**
     * 下载已经生成的代码,并且删除
     */
    public void downLoad(HttpServletRequest request, HttpServletRequest response) {
        String projectPath = System.getProperty("user.dir") + "/other/com";
        File file = new File(projectPath);
        if (file.exists()) {
            //打包

            //发送响应头

            //删除
        }
    }

    /**
     * 获取构建者库列表
     */
    public ResultPoJo<List<WeCreater>> allList(CreaterPo po) {
        ResultPoJo<List<WeCreater>> poJo = new ResultPoJo<>();

        Example<WeCreater> of = null;
        if (po.getHost() != null) {
            WeCreater weCreater = new WeCreater();
            weCreater.setHost(po.getHost());
            of = Example.of(weCreater);
        }

        List<WeCreater> all = weCreaterRepo.findAll(of);
        poJo.setResult(all);
        return poJo;
    }

    /**
     * 删除库列表
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultPoJo<ReturnCode> delete(CreaterPo po) {
        ResultPoJo<ReturnCode> poJo = new ResultPoJo<>();

        WeCreater weCreater = new WeCreater();
        weCreater.setHost(po.getHost());
        weCreaterRepo.delete(weCreater);

        poJo.setErrorStatus(ReturnCode.SUCCESS);
        return poJo;
    }

    /**
     * 构建者
     */
    public void builder(DataSourcePo po) {
        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        globalConfig.setOutputDir(projectPath + "/other");
        globalConfig.setAuthor("WeCreater");
        globalConfig.setOpen(false);
        globalConfig.setFileOverride(true);
        globalConfig.setSwagger2(true);
        globalConfig.setMapperName("%sRepo");
        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl(po.getUrl());
        dataSourceConfig.setDriverName(po.getDriverName());
        dataSourceConfig.setUsername(po.getUsername());
        dataSourceConfig.setPassword(po.getPassword());
        dataSourceConfig.setTypeConvert(new MariaDBTypeConvert());
        // 包配置
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(po.getParent());
        packageConfig.setMapper("repository");
        //模板配置
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setEntity("/entity.java.vm");
        templateConfig.setMapper("/repo.java.vm");
        if (!po.isWheSerCon()) {
            templateConfig.setService(null);
            templateConfig.setServiceImpl(null);
            templateConfig.setController(null);
        }
        if (!po.isWheXml()) {
            templateConfig.setXml(null);
        }

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        strategy.setSuperEntityClass(BaseJpaEntity.class);
        //判断是否生成所有的表
        if (po.isWheAll()) {
            strategy.setExclude(BlackListTables.OA.getLabel(), BlackListTables.OAT.getLabel(),
                    BlackListTables.OC.getLabel(), BlackListTables.OCD.getLabel(),
                    BlackListTables.OCT.getLabel(), BlackListTables.ORT.getLabel());
        } else {
            strategy.setInclude(po.getTables().split(","));
        }
        strategy.setEntitySerialVersionUID(true);
        strategy.setTablePrefix(packageConfig.getModuleName() + "_");

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        mpg.setGlobalConfig(globalConfig);
        mpg.setDataSource(dataSourceConfig);
        mpg.setPackageInfo(packageConfig);
        mpg.setStrategy(strategy);
        mpg.setTemplate(templateConfig);
        mpg.execute();
    }
}
