package com.we.wfc.creater.enums;

import com.we.wfc.common.base.BaseEnum;

public enum BlackListTables implements BaseEnum {

    OAT("oauth_access_token", "0"),

    OA("oauth_approvals", "1"),

    OCD("oauth_client_details", "2"),

    OCT("oauth_client_token", "3"),

    OC("oauth_code", "4"),

    ORT("oauth_refresh_token", "5"),

    ;

    private String label;
    private String value;

    BlackListTables(String label, String value) {

    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
