package com.we.wfc.system.service;

import com.we.wfc.common.base.BaseServiceImpl;
import com.we.wfc.system.entity.Area;
import com.we.wfc.system.repository.AreaRepo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 行政区划表 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-01-04
 */
@Service
@AllArgsConstructor
public class AreaService extends BaseServiceImpl<Area, String, AreaRepo> {

    private final AreaRepo areaRepo;

    public Page<Area> findAll(Specification<Area> query, PageRequest pageRequest) {
        return areaRepo.findAll(query, pageRequest);
    }
}
