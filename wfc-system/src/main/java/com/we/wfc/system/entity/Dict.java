package com.we.wfc.system.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.we.wfc.common.base.BaseJpaEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author WeCreater
 * @since 2020-02-09
 */
@Entity(name = "sys_dict")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysDict对象", description = "字典表")
public class Dict extends BaseJpaEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据值")
    @Column(length = 100)
    @TableField("value")
    private String value;

    @ApiModelProperty(value = "标签名")
    @Column(length = 100)
    @TableField("label")
    private String label;

    @ApiModelProperty(value = "类型")
    @Column(length = 100)
    @TableField("type")
    private String type;

    @ApiModelProperty(value = "描述")
    @Column(length = 100)
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "排序（升序）")
    @Column(length = 10)
    @TableField("sort")
    private BigDecimal sort;

    @ApiModelProperty(value = "父级编号")
    @Column(length = 64)
    @TableField("parent_id")
    private String parentId;

    @ApiModelProperty(value = "备注信息")
    @Column(length = 255)
    @TableField("remarks")
    private String remarks;


}
