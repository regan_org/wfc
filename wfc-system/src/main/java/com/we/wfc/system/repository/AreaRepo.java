package com.we.wfc.system.repository;


import com.we.wfc.system.entity.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * <p>
 * 行政区划 Repository 接口
 * </p>
 *
 * @author WeCreater
 * @since 2020-01-07
 */
public interface AreaRepo extends JpaRepository<Area, String>, JpaSpecificationExecutor<Area> {

}
