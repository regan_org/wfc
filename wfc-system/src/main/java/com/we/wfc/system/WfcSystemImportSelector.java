package com.we.wfc.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class WfcSystemImportSelector {

    public static void main(String[] args) {
        SpringApplication.run(WfcSystemImportSelector.class, args);
    }

}
