package com.we.wfc.system.controller;

import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.system.utils.DictUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Description: 字典服务对内服务接口
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/9 5:54 下午
 */
@ApiIgnore
@RestController
@RequestMapping("/dict/inner")
@AllArgsConstructor
public class InnerController {

    /**
     * 根据字典key和value获取字典标签
     */
    @GetMapping(value = "/getLabel")
    public String getDictLabel(DictTypeEnum dictTypeEnum, String value) {
        return DictUtil.getDictLabel(dictTypeEnum, value);
    }
}
