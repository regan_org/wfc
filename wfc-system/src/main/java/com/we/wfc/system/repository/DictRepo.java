package com.we.wfc.system.repository;


import com.we.wfc.system.entity.Dict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * <p>
 * 数据字典 Repository 接口
 * </p>
 *
 * @author WeCreater
 * @since 2020-01-07
 */
public interface DictRepo extends JpaRepository<Dict, String>, JpaSpecificationExecutor<Dict> {

}
