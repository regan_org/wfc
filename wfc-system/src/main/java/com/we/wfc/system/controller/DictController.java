package com.we.wfc.system.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.wenhao.jpa.Specifications;
import com.google.common.collect.Lists;
import com.we.wfc.common.base.Pagination;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.common.utils.CommonUtil;
import com.we.wfc.system.dto.DictQueryDto;
import com.we.wfc.system.dto.DictSaveDto;
import com.we.wfc.system.entity.Dict;
import com.we.wfc.system.service.DictService;
import com.we.wfc.system.utils.DictUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@RestController
@RequestMapping("/dict")
@Api(tags = "字典管理")
@AllArgsConstructor
public class DictController {

    @Autowired
    private DictService dictService;

    /**
     * 分页查询
     *
     * @return ResultPoJo
     */
    @GetMapping("")
    @ApiOperation(value = "分页查询", notes = "查询字典列表", produces = "application/json")
    public ResultPoJo<IPage<Dict>> getDictList4Page(Pagination pagination, DictQueryDto dictQueryDto) {
        //排序
        Page<Dict> page = dictService.findAll(
                dictQueryDto.buildQuery(),
                pagination.pageRequest(
                        Sort.by(Sort.Direction.DESC, "createDate")
                )
        );
        return ResultPoJo.ok(page);
    }


    /**
     * 根据id获取字典
     *
     * @return ResultPoJo
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询字典记录", notes = "根据id获取字典信息", produces = "application/json")
    public ResultPoJo<Dict> getDictById(@PathVariable String id) {
        return ResultPoJo.ok(dictService.findById(id));
    }

    /**
     * 验证字典类型是否重复
     *
     * @return
     */
    @GetMapping("/verify/type/repeat/{type}")
    @ApiOperation(value = "验证字典类型是否重复", notes = "flag: true【重复】，false【不重复】", produces = "application/json")
    public ResultPoJo repeatType(@PathVariable String type, @RequestParam(value = "id", required = false) String id) {

        //构建条件
        Specification<Dict> build = Specifications
                .<Dict>and()
                .eq(StrUtil.isNotBlank(type), "type", type)
                .ne(StrUtil.isNotBlank(id), "id", id)
                .build();
        //查询条件
        List<Dict> list = dictService.findAll(build);
        return ResultPoJo.ok(cn.hutool.core.lang.Dict.create()
                .set("flag", !list.isEmpty())
        );
    }


    /**
     * 保存字典记录
     *
     * @return ResultPoJo
     */
    @PostMapping("")
    @ApiOperation(value = "保存字典记录", notes = "保存字典记录", produces = "application/json")

    public ResultPoJo saveDict(@Validated @RequestBody DictSaveDto dictSaveDto) {
        Dict dict = dictSaveDto.convert();
        dict.setParentId(Optional.ofNullable(dict.getParentId()).orElse("0"));
        dictService.save(dict);
        DictUtil.clear(dict.getType());
        return ResultPoJo.ok();
    }

    /**
     * 更新字典记录
     *
     * @param dictSaveDto
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "更新字典记录", notes = "更新字典记录", produces = "application/json")
    public ResultPoJo updateDict(@PathVariable String id, @Validated @RequestBody DictSaveDto dictSaveDto) {
        Dict dict = (Dict) dictSaveDto.convert().setId(id);
        dictService.save(dict);
        DictUtil.clear(dict.getType());
        return ResultPoJo.ok();
    }

    /**
     * 删除字典
     *
     * @param id id
     * @return ResultPoJo
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除字典", notes = "删除字典", produces = "application/json")
    public ResultPoJo deleteDict(@PathVariable String id) {
        DictUtil.clear(dictService.findById(id).getType());
        dictService.deleteById4LogicInBatch(Lists.newArrayList(id.split(",")));
        return ResultPoJo.ok();
    }

    /**
     * 查询字典最大排序值
     *
     * @param parentId parentId
     * @return ResultPoJo
     */
    @GetMapping("/max/sort")
    @ApiOperation(value = "查询字典最大排序值", notes = "查询字典最大排序值", produces = "application/json")
    public ResultPoJo getMaxSort(@RequestParam(value = "parentId", required = false) String parentId) {

        //构建条件
        Specification<Dict> build = Specifications
                .<Dict>and()
                .eq(StrUtil.isNotBlank(parentId), "parentId", parentId)
                .build();
        List<Dict> all = dictService.findAll(build);
        BigDecimal maxSort = all.stream()
                .sorted((s1, s2) -> -s1.getSort().compareTo(s2.getSort()))
                .map(Dict::getSort)
                .findFirst().orElse(BigDecimal.ZERO);

        return ResultPoJo.ok(cn.hutool.core.lang.Dict.create()
                .set("maxSort", maxSort.add(new BigDecimal(10)))
        );
    }

    /**
     * 获取字典选择下拉框数据
     *
     * @return
     */
    @GetMapping("/select/data/{type}")
    @ApiOperation(value = "获取字典选择下拉框数据", notes = "获取字典选择下拉框数据", produces = "application/json")
    public ResultPoJo<List<cn.hutool.core.lang.Dict>> getDictSelectData(@PathVariable String type) {
        List<cn.hutool.core.lang.Dict> rsList =
                CommonUtil.convers(DictUtil.getDictList4Type(type),
                        dict -> cn.hutool.core.lang.Dict.create()
                                .set("label", dict.getLabel())
                                .set("value", dict.getValue())
                );
        return ResultPoJo.ok(rsList);
    }

    /**
     * 根据类型获取字典列表
     *
     * @return
     */
    @GetMapping("/list/by/type/{type}")
    @ApiOperation(value = "根据类型获取字典列表", notes = "", produces = "application/json")
    public ResultPoJo<List<Dict>> getDictListByType(@PathVariable String type) {
        List<Dict> dictList4Type = DictUtil.getDictList4Type(type);
        return ResultPoJo.ok(dictList4Type);
    }
}

