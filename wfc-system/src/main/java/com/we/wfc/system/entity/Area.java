package com.we.wfc.system.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.we.wfc.common.base.BaseJpaEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * <p>
 * 行政区划表
 * </p>
 *
 * @author WeCreater
 * @since 2020-02-09
 */
@Entity(name = "sys_area")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "SysArea对象", description = "行政区划表")
public class Area extends BaseJpaEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级编号")
    @Column(length = 64)
    @TableField("parent_id")
    private String parentId;

    @ApiModelProperty(value = "所有父级编号")
    @Column(length = 2000)
    @TableField("parent_ids")
    private String parentIds;

    @ApiModelProperty(value = "名称")
    @Column(length = 100)
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "简称")
    @Column(length = 100)
    @TableField("short_name")
    private String shortName;

    @ApiModelProperty(value = "排序")
    @Column(length = 10)
    @TableField("sort")
    private BigDecimal sort;

    @ApiModelProperty(value = "区域编码")
    @Column(length = 100)
    @TableField("code")
    private String code;

    @ApiModelProperty(value = "区域类型")
    @Column(length = 1)
    @TableField("type")
    private String type;

    @ApiModelProperty(value = "备注")
    @Column(length = 1000)
    @TableField("remarks")
    private String remarks;


}
