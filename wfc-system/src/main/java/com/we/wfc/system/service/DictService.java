package com.we.wfc.system.service;

import com.we.wfc.common.base.BaseServiceImpl;
import com.we.wfc.system.entity.Dict;
import com.we.wfc.system.repository.DictRepo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Service
@AllArgsConstructor
public class DictService extends BaseServiceImpl<Dict, String, DictRepo> {

    private final DictRepo dictRepo;

    public Page<Dict> findAll(Specification<Dict> query, PageRequest pageRequest) {
        return dictRepo.findAll(query, pageRequest);
    }


    public List<Dict> findAll(Specification<Dict> query) {
        return dictRepo.findAll(query);
    }
}
