package com.we.wfc.timer.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.we.wfc.common.serialize.OffsetDateTimeDeserializer;
import com.we.wfc.common.serialize.OffsetDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.OffsetDateTime;

/**
 * @Description: 触发器实体
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/6 11:56 下午
 */
@Data
public class TimerTrigger {

    @ApiModelProperty(value = "触发器分组")
    private String group;
    @ApiModelProperty(value = "触发器名称")
    private String name;

    @ApiModelProperty(value = "触发器描述")
    private String description;

    @ApiModelProperty(value = "下次触发时间")
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime nextFireTime;

    @ApiModelProperty(value = "上次触发时间")
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime preFireTime;

    @ApiModelProperty(value = "开始时间")
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime endTime;

    @ApiModelProperty(value = "优先级")
    private int priority;

    @ApiModelProperty(value = "触发器状态")
    private String state;
}
