package com.we.wfc.timer.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.quartz.Job;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;

/**
 * @Description: 定时实体类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
@Data
public class TimerJobOptions {
    /**
     * 无参构造器
     */
    public TimerJobOptions() {

    }

    /**
     * 构造器:任务名称和任务分组
     *
     * @param jobName
     * @param jobGroupName
     */
    public TimerJobOptions(String jobName, String jobGroupName, String jobClass) {
        this.jobName = jobName;
        this.jobGroupName = jobGroupName;
        this.jobClass = jobClass;
    }

    /**
     * 构造器:任务名称和任务分组
     *
     * @param jobName
     * @param jobGroupName
     */
    public TimerJobOptions(String jobName, String jobGroupName, Class<? extends Job> jobClass) {
        this.jobName = jobName;
        this.jobGroupName = jobGroupName;
        this.jobClass = jobClass.getName();
    }

    /**
     * 构造器:任务名称和任务分组
     *
     * @param jobName
     * @param jobGroupName
     */
    public TimerJobOptions(String jobName, String jobGroupName, String triggerName, String triggerGroupName, String jobClass) {
        this.jobName = jobName;
        this.jobGroupName = jobGroupName;
        this.triggerName = triggerName;
        this.triggerGroupName = triggerGroupName;
        this.jobClass = jobClass;
    }

    /**
     * 任务名称
     */
    @NotNull
    @ApiModelProperty(value = "任务名称", required = true)
    private String jobName;
    /**
     * 任务分组
     */
    @NotNull
    @ApiModelProperty(value = "任务分组", required = true)
    private String jobGroupName;
    /**
     * 触发器名称
     */
    @NotNull
    @ApiModelProperty(value = "触发器名称", required = true)
    private String triggerName;
    /**
     * 触发器分组
     */
    @NotNull
    @ApiModelProperty(value = "触发器分组", required = true)
    private String triggerGroupName;
    /**
     * 描述信息
     */
    @NotNull
    @ApiModelProperty(value = "任务描述信息", required = true)
    private String description;
    /**
     * 触发器描述信息
     */
    @NotNull
    @ApiModelProperty(value = "触发器描述信息", required = true)
    private String triggerDescription;
    /**
     * 任务类
     */
    @NotNull
    @ApiModelProperty(value = "任务类", required = true)
    private String jobClass;
    /**
     * 任务运行参数
     */
    @ApiModelProperty(value = "任务运行参数")
    private Map<String, Object> param;
    /**
     * 任务开始时间
     */
    @ApiModelProperty(value = "任务开始时间")
    private Date startTime;
    /**
     * 任务结束时间
     */
    @ApiModelProperty(value = "任务结束时间")
    private Date endTime;
    /**
     * 是否立刻运行一次
     */
    @ApiModelProperty(value = "是否立刻运行一次")
    private boolean startNow = false;
    /**
     * 优先级
     */
    @ApiModelProperty(value = "优先级")
    private Integer priority;
    /**
     * 是否持久化
     */
    @ApiModelProperty(value = "是否持久化")
    private boolean durability = false;

    /**
     * CRON表达式(仅CronJob有效)
     */
    @ApiModelProperty(value = "CRON表达式(仅CronJob有效)")
    private String cron;

    /**
     * 重复时间(单位:秒)(仅SimpleJob有效)
     */
    @ApiModelProperty(value = "重复时间(单位:秒)(仅SimpleJob有效)")
    private Integer intervalInSeconds;

    /**
     * 重复次数 小于0:无限次 大于1:重复执行N次(仅SimpleJob有效)
     */
    @ApiModelProperty(value = "重复次数 小于0:无限次 大于1:重复执行N次(仅SimpleJob有效)")
    private Integer repeatCount;

    /**
     * 自动记录执行日志
     */
    @ApiModelProperty(value = "自动记录执行日志")
    private boolean autoLogs = true;
}
