package com.we.wfc.timer.entity;

import org.quartz.Job;

/**
 * @Description: 设计为Job接口规范
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/6 11:56 下午
 */
public interface TimerJob extends Job {

    /**
     * 获取option
     *
     * @return
     */
    TimerJobOptions getOption();
}
