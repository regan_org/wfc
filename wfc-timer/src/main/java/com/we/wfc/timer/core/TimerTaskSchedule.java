package com.we.wfc.timer.core;

import com.alibaba.fastjson.JSONObject;
import com.we.wfc.common.annotation.LoadCompleted;
import com.we.wfc.common.annotation.SpringLoadCompleted;
import com.we.wfc.common.annotation.TimerSchedule;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.common.utils.SpringContextUtil;
import com.we.wfc.timer.entity.TimerJob;
import com.we.wfc.timer.entity.TimerJobOptions;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;

import java.util.Map;

/**
 * @Description: 定时任务初始化
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/7 12:01 上午
 */
@Slf4j
@LoadCompleted
@AllArgsConstructor
public class TimerTaskSchedule implements SpringLoadCompleted {

    private final TimerManager timerManager;

    /**
     * 容器加载完毕执行的定时任务初始化方法
     */
    @Override
    public void init(ApplicationContext context) {
        //如果timer管理方案没有配置则不进行
        if (ConverterUtil.isNotEmpty(timerManager) && timerManager.hasQuartzScheduler()) {
            //获取所有Timer的标识类
            Map<String, Object> beans =
                    SpringContextUtil.getApplicationContext().getBeansWithAnnotation(TimerSchedule.class);
            if (ConverterUtil.isNotEmpty(beans)) {
                beans.forEach((key, job) -> {
                    TimerSchedule timerJob = job.getClass().getAnnotation(TimerSchedule.class);
                    //只有标记了初始化启动的Job才会被加入到调度系统中
                    if (timerJob.bootload()) {
                        addJob((TimerJob) job);
                    }
                });
            }
        }
    }

    /**
     * 添加任务到调度系统
     */
    public void addJob(TimerJob jobBean) {
        TimerJobOptions option = jobBean.getOption();

        try {
            // 判断job是否存在，jobName和jboGroupName不能为空
            if (ConverterUtil.isNotEmpty(option.getJobName(), option.getJobGroupName())) {
                String optJson = JSONObject.toJSONString(option);
                log.debug("检测到定时任务： {}", optJson);
                boolean exists = timerManager.jobExists(option.getJobName(), option.getJobGroupName());
                // 任务不存在或可以覆盖就添加Job
                if (!exists) {
                    timerManager.addJob(option);
                    log.debug("添加定时任务(jobName={})成功： ", option.getJobName());
                }
                // 如果任务已经添加则不再添加
                if (exists) {
                    timerManager.addJob(option);
                    log.debug("覆盖定时任务(jobName={})成功： ", option.getJobName());
                }
            }

        } catch (SchedulerException e) {
            log.error("exception in TimerTaskSchedule.addJob", e);
        }
    }
}
