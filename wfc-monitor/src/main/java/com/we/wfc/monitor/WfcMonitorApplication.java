package com.we.wfc.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableAdminServer
@SpringBootApplication
@EnableDiscoveryClient
public class WfcMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(WfcMonitorApplication.class, args);
	}

}
