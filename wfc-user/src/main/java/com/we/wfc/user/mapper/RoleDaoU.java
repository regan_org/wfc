package com.we.wfc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.user.entity.Role;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Repository
public interface RoleDaoU extends BaseMapper<Role> {

}
