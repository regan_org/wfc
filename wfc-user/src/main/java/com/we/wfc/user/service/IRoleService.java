package com.we.wfc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.we.wfc.user.entity.Role;

import java.util.function.Function;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
public interface IRoleService extends IService<Role> {
    /**
     * 初始化
     * @return function
     */
    Function<Role, Role> preInit();
}
