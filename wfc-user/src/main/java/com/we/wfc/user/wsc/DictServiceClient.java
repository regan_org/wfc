package com.we.wfc.user.wsc;


import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.user.wsc.hystrix.DictServiceHystrix;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 字典服务WebServiceClient(wsc)
 * */
@ConditionalOnProperty("we.wsc.system")
@FeignClient( value = "${we.wsc.system}", fallback = DictServiceHystrix.class)
public interface DictServiceClient {

    /**
     * 根据字典key和value获取字典标签
     */
    @GetMapping(value = "/dict/inner/getLabel")
    String getDictLabel(@RequestParam DictTypeEnum dictTypeEnum, @RequestParam String value);
}
