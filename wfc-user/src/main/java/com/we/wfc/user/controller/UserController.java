package com.we.wfc.user.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Lists;
import com.we.wfc.common.base.BaseException;
import com.we.wfc.common.base.Pagination;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.common.utils.CommonUtil;
import com.we.wfc.user.dto.UserQueryDto;
import com.we.wfc.user.dto.UserSaveDto;
import com.we.wfc.user.entity.User;
import com.we.wfc.user.entity.validate.ValidateSave;
import com.we.wfc.user.service.IUserService;
import com.we.wfc.user.util.UserUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * <p>
 * 用户表  前端控制器
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户管理")
@AllArgsConstructor
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * 查询用户分页信息
     *
     * @param pagination
     * @return
     */
    @GetMapping("")
    @ApiOperation(value = "分页查询", notes = "分页查询", produces = "application/json")
    public ResultPoJo<IPage<User>> getUserList4Page(Pagination pagination, UserQueryDto userQueryDto) {
        IPage<User> page = userService.page(pagination.page(), userQueryDto.queryWrapper());
        //预处理
        CommonUtil.convers(page.getRecords(), userService.preInit());
        return ResultPoJo.ok(page);
    }


    /**
     * 根据id查询用户信息
     *
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "查询用户", notes = "根据id查询用户信息", produces = "application/json")
    public ResultPoJo<User> getUserById(@PathVariable String id) {
        return ResultPoJo.ok(userService.preInit().apply(UserUtil.getUser(id)));
    }

    /**
     * 获取当前登录用户
     * @return
     */
    @GetMapping("/get")
    @ApiOperation(value = "获取当前登录用户", notes = "获取当前登录用户", produces = "application/json")
    public ResultPoJo<User> getUser() {
        return ResultPoJo.ok(UserUtil.getUser());
    }

    /**
     * 保存用户信息
     *
     * @return
     */
    @PostMapping("")
    @ApiOperation(value = "保存用户", notes = "保存用户信息", produces = "application/json")
    public ResultPoJo saveUser(@Validated(ValidateSave.class) @RequestBody UserSaveDto userSaveDto) {
        userService.saveUser(userSaveDto);
        return ResultPoJo.ok();
    }

    /**
     * 更新用户信息
     *
     * @return
     */
    @PutMapping("/{id}")
    @ApiOperation(value = "修改用户", notes = "根据id修改用户信息", produces = "application/json")
    public ResultPoJo updateUser(@PathVariable String id, @RequestBody UserSaveDto userSaveDto) {
        userService.updateUser(id, userSaveDto);
        UserUtil.clear(id);
        return ResultPoJo.ok();
    }

    /**
     * 删除用户
     *
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除用户", notes = "根据id删除用户，支持批量删除，多用户用逗号间隔", produces = "application/json")
    public ResultPoJo deleteUser(@PathVariable String id) {
        userService.removeByIds(Lists.newArrayList(id.split(",")));
        UserUtil.clear(id.split(","));
        return ResultPoJo.ok();
    }

    /**
     * 修改密码
     *
     * @param oldPwd
     * @param newPwd
     * @return
     */
    @PutMapping("/update/pwd")
    @ApiOperation(value = "修改密码", notes = "修改密码", produces = "application/json")
    public ResultPoJo updatePwd(@RequestParam("oldPwd") String oldPwd, @RequestParam("newPwd") String newPwd) {
        User user = UserUtil.getUser();
        Optional.ofNullable(user).orElseThrow(() -> new BaseException(ReturnCode.NOT_FOUND_DATA));
        //比较密码是否正确
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (passwordEncoder.matches(oldPwd, user.getPassword())) {
            //update pwd
            user.setPassword(passwordEncoder.encode(newPwd)).updateById();
        } else {
            throw new BaseException(ReturnCode.PASSWORD_IS_INCORRECT);
        }
        UserUtil.clear(UserUtil.getCurrentUserId());
        return ResultPoJo.ok();
    }

    /**
     * 重置密码
     *
     * @return
     */
    @PutMapping("/reset/pwd/{id}")
    @ApiOperation(value = "重置密码", notes = "重置密码", produces = "application/json")
    public ResultPoJo resetPwd(@PathVariable String id) {
        if (UserUtil.isAdmin()) {
            for (String userId : id.split(",")) {
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                userService.updateById(new User().setId(userId).setPassword(passwordEncoder.encode("123456")));
                UserUtil.clear(userId);
            }
        } else {
            throw new BaseException(ReturnCode.NO_ACCESS_PERMISSION);
        }
        return ResultPoJo.ok();
    }
}

