package com.we.wfc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.user.entity.RoleMenu;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Repository
public interface RoleMenuDao extends BaseMapper<RoleMenu> {

}
