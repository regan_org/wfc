package com.we.wfc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.user.entity.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Repository
public interface MenuDao extends BaseMapper<Menu> {

    List<Menu> getMenu4User(String currentUserId);
}
