package com.we.wfc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.user.entity.Role;
import com.we.wfc.user.mapper.RoleDaoU;
import com.we.wfc.user.service.IRoleService;
import com.we.wfc.user.wsc.DictServiceClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Service
@AllArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleDaoU, Role> implements IRoleService {

    private final DictServiceClient dictServiceClient;

    /**
     * 初始化
     *
     * @return
     */
    @Override
    public Function<Role, Role> preInit() {
        return role -> {
            //查询角色类型
            role.set("roleTypeLabel", dictServiceClient.getDictLabel
                    (DictTypeEnum.ROLE_TYPE, role.getRoleType()));
            return role;
        };
    }
}
