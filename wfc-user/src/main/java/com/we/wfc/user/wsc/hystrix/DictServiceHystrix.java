package com.we.wfc.user.wsc.hystrix;

import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.user.wsc.DictServiceClient;
import org.springframework.stereotype.Component;

/**
 * @Description: 字典服务熔断
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/9 5:35 下午
 */
@Component
public class DictServiceHystrix implements DictServiceClient {

    @Override
    public String getDictLabel(DictTypeEnum dictTypeEnum, String value) {
        return null;
    }
}
