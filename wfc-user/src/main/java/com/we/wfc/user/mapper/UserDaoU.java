package com.we.wfc.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.we.wfc.user.entity.User;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表  Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@Repository
public interface UserDaoU extends BaseMapper<User> {

}
