package com.we.wfc.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.we.wfc.user.entity.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Repository
public interface IMenuService extends IService<Menu> {

    List<Menu> getMenu4User(String currentUserId);
}
