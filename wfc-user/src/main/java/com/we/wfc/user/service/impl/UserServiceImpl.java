package com.we.wfc.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.we.wfc.common.base.BaseException;
import com.we.wfc.common.enums.DictTypeEnum;
import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.user.dto.UserSaveDto;
import com.we.wfc.user.entity.User;
import com.we.wfc.user.entity.UserRole;
import com.we.wfc.user.mapper.UserDaoU;
import com.we.wfc.user.service.IRedisService;
import com.we.wfc.user.service.IUserService;
import com.we.wfc.user.wsc.DictServiceClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表  服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-03
 */
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserDaoU, User> implements IUserService {

    private final DictServiceClient dictServiceClient;

    @Override
    public Function<User, User> preInit() {
        return user -> {
            Optional.ofNullable(dictServiceClient.getDictLabel(DictTypeEnum.ROLE_TYPE, user.getUserType()))
                    .ifPresent(label -> user.set("userTypeLabel", label));
            List<UserRole> userRoles = new UserRole()
                    .selectList(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, user.getId()));
            user.set("roleIds", userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList()));
            return user;
        };
    }

    /**
     * 保存用户
     *
     * @param userSaveDto userSaveDto
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveUser(UserSaveDto userSaveDto) {
        //验证用户名是否存在
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>().eq(User::getLoginName, userSaveDto.getLoginName());
        List<User> list = list(queryWrapper);
        if (!list.isEmpty()) {
            throw new BaseException(ReturnCode.USER_IS_NOT_EXIST);
        }
        User user = userSaveDto.convert();
        user.preInsert();
        save(user);
        // 插入角色
        for (String roleId : userSaveDto.getRoleId().split(",")) {
            new UserRole().setRoleId(roleId).setUserId(user.getId()).insert();
        }
    }

    /**
     * 更新用户
     *
     * @param id          id
     * @param userSaveDto u
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUser(String id, UserSaveDto userSaveDto) {
        //验证用户名是否存在
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getLoginName, userSaveDto.getLoginName())
                .ne(User::getId, id);
        List<User> list = list(queryWrapper);
        if (!list.isEmpty()) {
            throw new BaseException(ReturnCode.USER_IS_NOT_EXIST);
        }
        User user = userSaveDto.convert();
        user.setId(id);
        user.preUpdate();
        updateById(user);
        //刷新角色信息
        new UserRole().delete(new LambdaQueryWrapper<UserRole>().eq(UserRole::getUserId, user.getId()));
        // 插入角色
        for (String roleId : userSaveDto.getRoleId().split(",")) {
            new UserRole().setRoleId(roleId).setUserId(user.getId()).insert();
        }
    }
}
