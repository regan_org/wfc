package com.we.wfc.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.we.wfc.user.entity.Menu;
import com.we.wfc.user.mapper.MenuDao;
import com.we.wfc.user.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuDao, Menu> implements IMenuService {
    @Autowired
    MenuDao menuDao;

    /**
     * 根据用户查询菜单
     * @param currentUserId currentUserId
     * @return List<Menu>
     */
    @Override
    public List<Menu> getMenu4User(String currentUserId) {
        return menuDao.getMenu4User(currentUserId);
    }
}
