package com.we.wfc.user.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.we.wfc.common.cache.CacheKey;
import com.we.wfc.common.constants.BaseConstants;
import com.we.wfc.common.utils.CommonUtil;
import com.we.wfc.common.utils.SpringContextUtil;
import com.we.wfc.user.entity.User;
import com.we.wfc.user.entity.enums.RoleTypeEnum;
import com.we.wfc.user.service.IRedisService;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 用户工具类
 *
 * @author zhangby
 * @date 27/9/19 5:32 pm
 */
public class UserUtil {

    /**
     * redis service
     */
    private static IRedisService redisService = SpringContextUtil.getBean(IRedisService.class);

    /**
     * 获取当前访问用户id
     *
     * @return
     */
    public static String getCurrentUserId() {
        String userId = System.getProperty(BaseConstants.CURRENT_USER_ID);
        return Optional.ofNullable(userId).orElseGet(()->
            CommonUtil.resolve(() -> {
                String tokenValue = ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getTokenValue();
                Claims claims = CommonUtil.parseJWT(tokenValue);
                Object uid = null;
                if (claims != null) {
                    uid = claims.get("user_id");
                }
                return Optional.ofNullable(uid).map(String::valueOf).filter(StrUtil::isNotBlank).orElse(null);
            }).orElse(null)
        );
    }

    /**
     * 获取当前用户授权角色
     *
     * @return
     */
    public static List<String> getAuthorities() {
        Collection<? extends GrantedAuthority> authorities = Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getAuthorities)
                .orElse(null);
        return Optional.ofNullable(authorities).map(li -> li.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())).orElse(Lists.newArrayList());
    }

    /**
     * 系统管理员
     * @return
     */
    public static Boolean isAdmin() {
        List<String> authorities = getAuthorities();
        return authorities.contains(RoleTypeEnum.admin.toString());
    }

    /**
     * 获取当前登录用户
     * @return
     */
    public static User getUser() {
        return getUser(getCurrentUserId());
    }

    /**
     * 获取当前登录用户
     * @param id id
     * @return
     */
    public static User getUser(String id) {
        return CommonUtil.emptyStr(id).map(userId -> {
            //查redis
            String redisKey = StrUtil.format(CacheKey.REDIS_KEY_DICT_TYPE, id);
            User user = redisService.getBean(redisKey, User.class);
            //如果为空，查询数据库
            if (ObjectUtil.isNull(user)) {
                user = new User().selectById(id);
                //存入数据库
                redisService.set(redisKey, user);
            }
            return user;
        }).orElse(null);
    }

    /**
     * 清除缓存
     * @param id id
     * @return
     */
    public static void clear(String... id) {
        for (String userId : id) {
            String redisKey = StrUtil.format(CacheKey.REDIS_KEY_USER_ID, userId);
            redisService.remove(redisKey);
        }
    }
}
