package com.we.wfc.user.entity;

import lombok.Data;

/**
 * 获取授权封装数据
 *
 * @author zhangby
 * @date 2019/2/27 2:29 PM
 */

@Data
public class JWT {
    private String access_token;
    private String token_type;
    private String refresh_token;
    private int expires_in;
    private String scope;
    private String jti;
}
