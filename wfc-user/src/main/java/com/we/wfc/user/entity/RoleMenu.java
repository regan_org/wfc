package com.we.wfc.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.we.wfc.common.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author zhangby
 * @since 2019-12-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role_menu")
@ApiModel(value="RoleMenu对象", description="角色菜单表")
public class RoleMenu extends BaseEntity<RoleMenu> {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "角色编号")
    private String roleId;

    @ApiModelProperty(value = "菜单编号")
    private String menuId;


    @Override
    protected Serializable pkVal() {
        return this.roleId;
    }

}
