package com.we.wfc.sender.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description:
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 4:08 下午
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "we.cmu.sms")
public class SmsProp {

    /**
     * AppKey
     */
    private String appkey;

    /**
     * App安全码
     */
    private String masterSecret;

    /**
     * 开发账号Key
     */
    private String devKey;

    /**
     * 开发账号安全码
     */
    private String devSecret;
}
