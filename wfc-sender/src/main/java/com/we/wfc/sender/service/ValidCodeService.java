package com.we.wfc.sender.service;

import cn.hutool.core.lang.Validator;
import com.we.wfc.common.cache.CacheKey;
import com.we.wfc.common.cache.Jedis.JedisImpl;
import com.we.wfc.common.constants.BaseConstants;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.sender.bo.SmsBo;
import com.we.wfc.sender.enums.EmailMsgKey;
import com.we.wfc.sender.po.UnifiedSmsPo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Description: 验证码业务
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/24 6:07 下午
 */
@Slf4j
@Service
@AllArgsConstructor
public class ValidCodeService {

    private final JedisImpl jedis;

    private final SmsService smsService;

    private final EmailSendService emailSendService;

    /**
     * 校验短信验证码是否正确(不自动清除redis)
     *
     * @param phone 手机号
     * @param code  验证码
     * @return
     */
    public Boolean checkSMSCode(String phone, String code) {
        return checkCode(phone, code, false);
    }

    /**
     * 校验短信验证码是否正确
     *
     * @param phone      手机号
     * @param code       验证码
     * @param rightClean 正确就清除
     * @return
     */
    public Boolean checkSMSCode(String phone, String code, boolean rightClean) {
        return checkCode(phone, code, rightClean);
    }

    /**
     * 验证手机号是否已经注册,以及号码和手机验证码是否匹配
     * ps:发送内容为验证码的情况下
     *
     * @param mobile     手机号
     * @param validCode  验证码
     * @param rightClean 如果正确是否清除
     */
    public Boolean checkCode(String mobile, String validCode, boolean rightClean) {
        boolean res = false;
        //取出redis当中存储的code码
        String mobileKey = CacheKey.getPN(mobile);
        SmsBo serialize = jedis.getSerialize(mobileKey, SmsBo.class);
        //查询验证码是否已经过期或者不正确
        if (ConverterUtil.isNotEmpty(serialize) ||
                serialize.getExprire().compareTo(new Date()) > 0 ||
                validCode.equals(serialize.getSendMsg())) {
            res = true;
        }

        //如果需要清除
        if (rightClean) {
            //改为过期
            serialize.setExprire(new Date());
            Long expire = jedis.getExpire(mobileKey);
            jedis.setSerializeExpire(mobileKey, serialize, expire.intValue());
        }

        return res;
    }

    /**
     * 发送验证码
     *
     * @param account 用户账号
     * @param length  验证码长度
     * @param seconds 过期时间
     * @return
     */
    public Boolean sendValidCode(String account, String code, int length, int seconds) {

        //RedisKey
        String cmuKey = null;
        boolean res = false;
        Boolean sendFlag = false;

        //如果为空就生成随机数
        if (code == null) {
            code = ConverterUtil.getCheckCode(ConverterUtil.RANDOM_TYPE_NUM, length);
        }
        //判断账户类型是否为手机
        if (Validator.isMobile(account)) {
            //发送手机短信业务逻辑
            cmuKey = CacheKey.getPN(account);
            UnifiedSmsPo unifiedSmsPo = new UnifiedSmsPo(177410, account, "code", code);
            sendFlag = smsService.sendSms(unifiedSmsPo);
        } else {
            //发送邮件业务逻辑
            String content = EmailMsgKey.EMAIL_KEY.getLabel() + code;
            emailSendService.sendTextMail(account, BaseConstants.TITLE_TEXT_VAILDCODE, content);
            cmuKey = CacheKey.getEm(account);
        }

        //如果发送成功就插入到缓存中并设置过期时间
        if (sendFlag) {
            log.info("Account: " + account + " sent" + "msg is " + code);
            //Redis入参
            res = jedis.setSerializeExpire(cmuKey, code, seconds);
        }

        return res;
    }
}
