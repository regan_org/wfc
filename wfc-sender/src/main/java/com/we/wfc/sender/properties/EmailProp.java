package com.we.wfc.sender.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description: 邮箱配置
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 4:08 下午
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "we.cmu.mail")
public class EmailProp {

    /**
     * 发件人账号密码
     */
    public String userName;

    /**
     * 授权码
     * */
    public String passWord;

    /**
     * 邮件服务器
     */
    public String host;

    /**
     * 端口
     * 25为标准的smtp协议端口
     */
    public int port;

    /**
     * 发件人
     * */
    public String from;
}
