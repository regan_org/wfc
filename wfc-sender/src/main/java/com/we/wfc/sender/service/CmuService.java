package com.we.wfc.sender.service;

import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.pojo.ResultPoJo;
import com.we.wfc.sender.properties.CmuProp;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * @Description: 电信服务接口业务层
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/12 11:46 上午
 */
@Slf4j
@Service
@AllArgsConstructor
@EnableConfigurationProperties({CmuProp.class})
public class CmuService {

    private final CmuProp cmuProp;

    private final ValidCodeService validCodeService;

    /**
     * 通信账号发送验证码
     */
    public ResultPoJo<ReturnCode> sendCode(String account) {

        //定义参数
        ResultPoJo<ReturnCode> poJo = new ResultPoJo<>();
        String code = null;

        //如何不是测试发送
        if (cmuProp.isCmuTest()) {
            code = "6666";
        }

        validCodeService.sendValidCode(account, code, 4, cmuProp.getExprire());

        //返回对象入参
        poJo.setErrorStatus(ReturnCode.SUCCESS);
        return poJo;
    }
}
