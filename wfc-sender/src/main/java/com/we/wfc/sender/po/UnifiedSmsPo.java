package com.we.wfc.sender.po;

import lombok.Data;

/**
 * @Description: 极光短信统一入参类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2020/2/26 5:17 下午
 */
@Data
public class UnifiedSmsPo {

    /**
     * 签名ID
     */
    private int signId;

    /**
     * 模板ID
     */
    private int tempId;

    /**
     * 手机号
     */
    private String phoneNum;

    /**
     * 模板变量key as:{{code}}
     */
    private String paramKey;

    /**
     * 模板变量值
     */
    private String paramValue;

    /**
     * 定时参数
     * 格式为 yyyy-MM-dd HH:mm:ss
     */
    private String sendTime;


    /**
     * 非定时默认签名
     */
    public UnifiedSmsPo(int tempId, String phoneNum, String paramKey, String paramValue) {
        this.tempId = tempId;
        this.phoneNum = phoneNum;
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }

    /**
     * 非定时指定签名
     */
    public UnifiedSmsPo(int signId, int tempId, String phoneNum, String paramKey, String paramValue) {
        this.signId = signId;
        this.tempId = tempId;
        this.phoneNum = phoneNum;
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }

    /**
     * 定时默认签名
     */
    public UnifiedSmsPo(int tempId, String phoneNum, String tmpKey, String paramKey, String paramValue) {
        this.tempId = tempId;
        this.phoneNum = phoneNum;
        this.paramKey = paramKey;
        this.paramValue = paramValue;
        this.sendTime = sendTime;
    }

    /**
     * 定时指定签名
     */
    public UnifiedSmsPo(int signId, int tempId, String phoneNum, String tmpKey, String paramKey, String paramValue) {
        this.signId = signId;
        this.tempId = tempId;
        this.phoneNum = phoneNum;
        this.paramKey = paramKey;
        this.paramValue = paramValue;
        this.sendTime = sendTime;
    }

    public UnifiedSmsPo() {
    }
}
