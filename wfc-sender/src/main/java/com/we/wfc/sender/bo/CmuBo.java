package com.we.wfc.sender.bo;

import com.we.wfc.common.utils.ConverterUtil;
import lombok.Getter;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @Description: 通信账号信息BO
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/12 12:07 下午
 */
@Getter
public class CmuBo implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = -7335744666879829410L;

    /**
     * 发送的通信账号
     */
    private String account;

    /**
     * 发送的内容
     */
    private String sendMsg;

    /**
     * 过期时间(秒)若为负数则不会过期
     */
    private Date exprire;

    public void setAccount(String account) {
        this.account = account;
    }

    public void setSendMsg(String sendMsg) {
        this.sendMsg = sendMsg;
    }

    public void setExprire(int expireSecond) {
        if (expireSecond >= 0) {
            this.exprire = ConverterUtil.addDate(new Date(), Calendar.SECOND, expireSecond);
        }
    }

}
