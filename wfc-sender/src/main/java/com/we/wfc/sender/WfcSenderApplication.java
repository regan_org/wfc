package com.we.wfc.sender;

import com.we.wfc.common.annotation.CORS;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableResourceServer
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.we.wfc.common", "com.we.wfc.sender", "com.we.wfc.security"})
@EntityScan({"com.we.wfc.common.entity", "com.we.wfc.security.entity"})
public class WfcSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(WfcSenderApplication.class, args);
    }

}
