package com.we.wfc.sender.bo;

import com.we.wfc.common.utils.ConverterUtil;
import lombok.Getter;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @Description: 短信业务对象
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 7:24 下午
 */
@Getter
public class SmsBo implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = -7335744666873269410L;

    /**
     * 发送的手机号
     */
    private String mobile;

    /**
     * 发送的内容
     */
    private String sendMsg;

    /**
     * 过期时间(秒)若为负数则不会过期
     */
    private Date exprire;

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setSendMsg(String sendMsg) {
        this.sendMsg = sendMsg;
    }

    public void setExprire(int expireSecond) {
        if (expireSecond >= 0) {
            this.exprire = ConverterUtil.addDate(new Date(), Calendar.SECOND, expireSecond);
        }
    }

    public void setExprire(Date exprire) {
        this.exprire = exprire;
    }
}
