package com.we.wfc.common.config;


import com.we.wfc.common.annotation.CORS;
import com.we.wfc.common.filter.CORSFilter;
import com.we.wfc.common.utils.ConverterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Map;

/**
 * @Description: 跨域处理配置类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 5:55 下午
 */
public class CorsAutoConfiguration implements ImportBeanDefinitionRegistrar, EnvironmentAware {

    @Autowired
    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        Map<String, Object> defaultAttrs = metadata.getAnnotationAttributes(CORS.class.getName(), true);

        if (defaultAttrs != null) {
            String name = "";
            if (metadata.hasEnclosingClass()) {
                name = "default." + metadata.getEnclosingClassName();
            } else {
                name = "default." + metadata.getClassName();
            }
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(CORSFilter.class);

            // 跨域配置
            UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
            CorsConfiguration corsConfiguration = new CorsConfiguration();
            corsConfiguration.setAllowCredentials(true);
            String origin = ConverterUtil.toString(environment.getProperty("we.cors.origin"), defaultAttrs.get("origin").toString());
            corsConfiguration.addAllowedOrigin(origin);
            String header = ConverterUtil.toString(environment.getProperty("we.cors.header"), ConverterUtil.toString(defaultAttrs.get("header")));
            corsConfiguration.addAllowedHeader(header);
            String method = ConverterUtil.toString(environment.getProperty("we.cors.method"), ConverterUtil.toString(defaultAttrs.get("method")));
            corsConfiguration.addAllowedMethod(method);
            String allowUrl = ConverterUtil.toString(environment.getProperty("we.cors.allowUrl"), ConverterUtil.toString(defaultAttrs.get("allowUrl")));
            urlBasedCorsConfigurationSource.registerCorsConfiguration(allowUrl, corsConfiguration);
            // CorsFilter构造函数
            builder.addConstructorArgValue(urlBasedCorsConfigurationSource);
            builder.addConstructorArgValue(corsConfiguration);

            AbstractBeanDefinition definition = builder.getBeanDefinition();
            // 注册跨域的CorsFilter
            registry.registerBeanDefinition(name + "." + CORSFilter.class.getSimpleName(), definition);
        }

    }

}
