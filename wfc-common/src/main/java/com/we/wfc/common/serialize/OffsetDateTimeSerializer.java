package com.we.wfc.common.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description: OffsetDateTime序列化
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/20 11:34 上午
 */
public class OffsetDateTimeSerializer extends JsonSerializer<OffsetDateTime> {
    public static final OffsetDateTimeSerializer INSTANCE = new OffsetDateTimeSerializer();

    @Override
    public void serialize(OffsetDateTime dateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(getSerializeStr(dateTime));
    }

    public String getSerializeStr(OffsetDateTime dateTime) {
        if (null == dateTime) {
            return null;
        }
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
