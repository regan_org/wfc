package com.we.wfc.common.base;

import com.we.wfc.common.base.BaseJpaEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * 封装BaseService
 *
 * @param <T>
 * @param <ID>
 * @param <R>
 */
public interface BaseService<T extends BaseJpaEntity, ID extends Serializable, R extends JpaRepository<T, ID>> {

    /**
     * 查询单条记录
     */
    T findById(ID id);

    boolean existsById(ID var1);

    List<T> findAll();

    List<T> findAll(Sort sort);

    Optional<T> findOne(Example<T> var1);

    List<T> findAll(Example<T> var1);

    List<T> findAll(Example<T> var1, Sort var2);

    Page<T> findAll(Example<T> var1, Pageable var2);

    Page<T> findAll(Pageable var1);

    long count(Example<T> var1);

    boolean exists(Example<T> var1);

    long count();

    /**
     * 保存记录
     */
    T save(T t);

    /**
     * 批量保存
     */
    List<T> saveAll(List<T> var1);

    /**
     * 物理删除
     */
    void deleteById(ID var1);

    void deleteInBatch(List<T> entities);

    /**
     * 逻辑删除
     */
    void deleteById4Logic(ID var1);

    /**
     * 批量逻辑删除
     */
    void deleteById4LogicInBatch(List<ID> entities);
}
