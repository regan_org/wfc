package com.we.wfc.common.enums;

import com.google.common.collect.Lists;
import com.we.wfc.common.exception.WeReturn;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wangjiahao
 * @version 1.0
 * @since 2019/10/23 10:10 上午
 */
@AllArgsConstructor
public enum ReturnCode implements WeReturn {
    /**
     * 其他
     * 正向->00000
     * 负向->90000
     * */
    SUCCESS("00000", "请求成功"),
    FAIL("90000", "请求失败"),
    SERVER_EXCEPTION("90001", "服务器忙碌,请稍后再试~"),
    NOT_FOUND_DATA("90002", "未查询到数据"),
    PARAMETER_TYPE_IS_INCORRECT("90003", "参数类型不正确"),
    MISSING_PARAMETERS("90004", "缺少参数"),
    HTTP_METHOD_NOT_SUPPORT("90005", "http方法不支持"),
    AUTHORIZATION_FAILED("90006", "授权失败"),
    NO_ACCESS_PERMISSION("90007", "没有访问权限"),
    REPEAT_OPERATION("90008", "系统太累了,客官您歇会~"),

    /**
     * Sender
     * 正向-> 10000
     * 负向-> 85000
     * */
    VERIFICATION_CODE_MATCHES_THE_PHONE("10001","验证码与手机匹配成功"),
    VERIFICATION_EXPIRED_OR_MISMATCH("85001", "验证码已过期或不匹配"),

    /**
     * File
     * 正向-> 15000
     * 负向-> 80000
     * */
    FILE_SIZE_EXCEEDS_MAXIMUM("80002", "文件大小超过上限"),
    FILE_TYPE_IS_NOT_ALLOWED("80003", "文件类型不被允许"),
    EXCEL_DOWNLOAD_FAILED("80004", "Excel下载失败"),
    DOWNLOAD_FAILURE_NOT_SET_RESPONSE("80005", "下载失败未设置Response"),

    /**
     * Creater
     * 正向-> 20000
     * 负向-> 75000
     * */

    /**
     * User
     * 正向-> 25000
     * 负向-> 70000
     * */
    LOGIN_SUCCESS("25001", "登陆成功"),
    REGISTER_SUCCESS("25002", "注册成功"),
    NOT_FOUND_USER("70001", "未查询到用户"),
    USER_IS_NOT_EXIST("70002", "用户不存在"),
    LOGIN_FAIL("70003", "登陆失败,请稍后重试"),
    INSUFFICIENT_PERMISSIONS("70004", "权限不足"),
    PASSWORD_IS_INCORRECT("70005", "密码不正确"),
    ENGLISH_NAME_ALREADY_EXISTS("70006", "英文名已存在"),
    PARENT_RECORD_NOT_EXIST("70007", "父级记录不存在"),

    /**
     * Timer
     * 正向-> 30000
     * 负向-> 65000
     * */

    /**
     * System
     * 正向-> 35000
     * 负向-> 60000
     * */

    ;
    private String code;
    private String msg;

    /**
     * 将枚举code转成list
     */
    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = Lists.newArrayList();
        for (ReturnCode rc : ReturnCode.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", rc.getCode());
            map.put("msg", rc.getMsg());
            list.add(map);
        }
        return list;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
