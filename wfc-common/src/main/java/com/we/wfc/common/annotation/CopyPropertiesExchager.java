package com.we.wfc.common.annotation;

public interface CopyPropertiesExchager {
    /**
     * 值转换器
     * 
     * @param oldVal
     * @param newVal
     * @return
     */
    <T> T exchage(T oldVal, T newVal);
}
