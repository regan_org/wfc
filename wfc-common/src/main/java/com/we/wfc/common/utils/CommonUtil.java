package com.we.wfc.common.utils;

import cn.hutool.core.util.StrUtil;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.we.wfc.common.constants.BaseConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 常用工具类
 *
 * @author zhangby
 * @date 2019-05-13 12:20
 */
public class CommonUtil {

    /**
     * 获取当前登录用户
     * @return
     */
    public static String getCurrentId() {
        return Optional.ofNullable(System.getProperty(BaseConstants.CURRENT_USER_ID))
                .filter(StrUtil::isNotBlank).orElse(null);
    }

    /**
     * 解析token
     *
     * @return
     */
    public static Claims parseJwtNoBearer(String jwt) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(BaseConstants.JWT_KEY)
                    .parseClaimsJws(jwt).getBody();
            return claims;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 解析token
     *
     * @param jsonWebToken
     * @param base64Security
     * @return
     */
    public static Claims parseJWT(String jsonWebToken, String base64Security) {
        String jwt = jsonWebToken.replace("Bearer ", "");
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(base64Security.getBytes())
                    .parseClaimsJws(jwt).getBody();
            return claims;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 解析token
     *
     * @param jsonWebToken
     * @return
     */
    public static Claims parseJWT(String jsonWebToken) {
        return emptyStr(jsonWebToken).map(token -> CommonUtil.parseJWT(token, BaseConstants.JWT_KEY)).orElse(null);
    }

    /**
     * 字符串模板替换 截取
     * ClassTest::getDictList4Function,{}::{} ->[ClassTest,getDictList4Function]
     *
     * @param str
     * @param temp
     * @return
     */
    public static List<String> splitStr4Temp(String str, String temp) {
        List<String> rsList = Lists.newArrayList();
        Iterator<String> iterator = Splitter.on("{}").omitEmptyStrings().split(temp).iterator();
        while (iterator.hasNext()) {
            str = str.replace(iterator.next(), "〆");
        }
        Iterator<String> split = Splitter.on("〆").omitEmptyStrings().split(str).iterator();
        while (split.hasNext()) {
            rsList.add(split.next());
        }
        return rsList.stream().filter(StrUtil::isNotBlank).collect(Collectors.toList());
    }

    /**
     * list数据转换
     *
     * @param list list对象
     * @param func lamdba 表达式 function
     * @param <E>  原对象
     * @param <T>  转换完的对象
     * @return List<E>
     */
    public static <E, T> List<E> convers(List<T> list, Function<T, E> func) {
        return list.stream().collect(ArrayList::new, (li, p) -> li.add(func.apply(p)), List::addAll);
    }

    /**
     * 异常捕获
     *
     * @param resolver resolver
     * @param <T>      T
     * @return
     */
    public static <T> Optional<T> resolve(Supplier<T> resolver) {
        Optional<T> optional = Optional.empty();
        try {
            T result = resolver.get();
            optional = Optional.ofNullable(result);
        } catch (Exception e) {
            optional = Optional.empty();
        }
        return optional;
    }

    public static Optional<String> emptyStr(String string) {
        return Optional.ofNullable(string).filter(StrUtil::isNotBlank);
    }
}
