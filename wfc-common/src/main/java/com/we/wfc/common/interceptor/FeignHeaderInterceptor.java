package com.we.wfc.common.interceptor;

import com.google.common.net.HttpHeaders;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

/**
 * feign 请求统一处理token拦截器问题
 * 
 * @author zhangby
 * @date 2019-04-01 17:01
 */
@Component
public class FeignHeaderInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        template.header(HttpHeaders.AUTHORIZATION, System.getProperty(HttpHeaders.AUTHORIZATION));
    }
}