package com.we.wfc.common.cache.Jedis;

import com.alibaba.fastjson.JSON;
import com.we.wfc.common.cache.JedisDao;
import com.we.wfc.common.utils.ConverterUtil;
import com.we.wfc.common.utils.SerializeUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @Description: Jedis封装实现类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 11:44 上午
 */
@Slf4j
@Component("jedisImpl")
@AllArgsConstructor
@EnableConfigurationProperties(value = {RedisProperties.class})
public class JedisImpl implements JedisDao {

    private final JedisPool jedisPool;

    private final RedisProperties redisProperties;

    public Jedis getResource() {
        Jedis jedis = jedisPool.getResource();
        jedis.select(redisProperties.getDatabase());
        return jedis;
    }

    /**
     * 取得字符串Redis数据
     *
     * @param key 键
     * @return
     */
    @Override
    public synchronized String getStr(String key) {
        String result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.get(key);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in getStr", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao getStr key:" + key);
        return result;
    }

    /**
     * 设置字符串Redis数据
     *
     * @param key   键
     * @param value 值
     * @return
     */
    @Override
    public synchronized boolean setStr(String key, String value) {
        if (ConverterUtil.isEmpty(key)) {
            return false;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.set(key, value);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in setStr", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao setStr key:" + key);
        return true;
    }

    /**
     * 设置可过期的字符串Redis数据
     *
     * @param key     键
     * @param value   值
     * @param seconds 过期时间(秒)
     * @return
     */
    @Override
    public synchronized boolean setStrExpire(String key, String value, int seconds) {
        if (ConverterUtil.isEmpty(key)) {
            return false;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.setex(key, seconds, value);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in setStrExpire", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao setStrExpire key:" + key);
        return true;
    }

    /**
     * 删除字符串Redis数据
     *
     * @param keys 键
     * @return
     */
    @Override
    public synchronized Long delStr(String... keys) {
        long result = 0L;
        if (null == keys) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.del(keys);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in delStr", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao delStr keys:" + String.join(",", keys));
        return result;
    }

    /**
     * 取得序列化Redis数据
     *
     * @param key   键
     * @param clazz 实体类型
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public synchronized <T> T getSerialize(String key, Class<T> clazz) {
        if (ConverterUtil.isEmpty(key)) {
            return null;
        }
        Object obj = null;
        Jedis jedis = null;
        try {
            jedis = getResource();

            if (null == jedis.get(key.getBytes())) {
                logger.debug("JedisDao getSerialize key: " + key + " return null object");
                return null;
            }
            obj = SerializeUtil.deserialize(jedis.get(key.getBytes()));
        } catch (Exception e) {
            logger.error("Exception in getSerialize", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao getSerialize key:" + key);
        if (null == obj) {
            return null;
        }
        return (T) obj;
    }

    /**
     * 设置序列表Redis数据
     *
     * @param key 键
     * @param obj 实体
     * @return
     */
    @Override
    public synchronized boolean setSerialize(String key, Object obj) {
        if (ConverterUtil.isEmpty(key)) {
            return false;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.set(key.getBytes(), SerializeUtil.serialize(obj));
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in setSerialize", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao setSerialize key:" + key);
        return true;
    }

    /**
     * 设置可过期的字符串Redis数据
     *
     * @param key     键
     * @param value   值
     * @param seconds 过期时间(秒)
     * @return
     */
    @Override
    public synchronized boolean setSerializeExpire(String key, Object value, int seconds) {
        if (ConverterUtil.isEmpty(key)) {
            return false;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.setex(key.getBytes(), seconds, SerializeUtil.serialize(value));
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in setSerializeExpire", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao setSerializeExpire key:" + key);
        return true;
    }

    /**
     * 删除序列化Redis数据
     *
     * @param keys
     * @return
     */
    @Override
    public synchronized Long delSerialize(String... keys) {
        Long result = 0L;
        if (null == keys) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            for (String key : keys) {
                result += jedis.del(key.getBytes());
            }
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in delSerialize", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao delSerialize param:" + String.join(",", keys));
        return result;
    }

    /**
     * 取得Redis中的字符串key
     *
     * @param patten 表达式
     * @return
     */
    @Override
    public synchronized Set<String> getKeys(String patten) {
        Set<String> result = null;
        if (ConverterUtil.isEmpty(patten)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.keys(patten);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in getKeys", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao getKeys patten:" + patten);
        return result;
    }

    /**
     * 取得Redis中的比特key
     *
     * @param patten 表达式
     * @return
     */
    @Override
    public synchronized Set<byte[]> getByteKeys(String patten) {
        Set<byte[]> result = null;
        if (ConverterUtil.isEmpty(patten)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.keys(patten.getBytes());
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in getKeys", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao getKeys patten:" + patten);
        return result;
    }

    /**
     * 取得Redis中某字符串key的过期时间
     *
     * @param key
     * @return
     */
    @Override
    public long getExpire(String key) {
        long result = 0L;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.ttl(key);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in getExpire", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao getExpire key:" + key + " with:" + result + " seconds");
        return result;
    }

    /**
     * 设置Redis中某字符串key的过期时间
     *
     * @param key     缓存key
     * @param seconds 过期时间 单位：秒
     * @return
     */
    @Override
    public long setExpire(String key, int seconds) {
        long result = 0;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            if (jedis.exists(key)) {
                result = jedis.expire(key, seconds);
            } else {
                logger.debug("JedisDao setExpire key:" + key + " this key is not exists");
                return result;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception in setExpire", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao setExpire key:" + key + " result:" + result);
        return result;
    }

    /**
     * 判断key是否存在
     *
     * @param key
     * @return
     */
    @Override
    public Boolean exists(String key) {
        Boolean result = false;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.exists(key);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in exists", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao exists key:" + key + " result:" + result);
        return result;
    }

    /**
     * 将key中储存的数字值增一
     *
     * @param key
     * @return
     */
    @Override
    public Long incr(String key) {
        Long result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.incr(key);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in incr", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao incr key:" + key + " result:" + result);
        return result;
    }

    /**
     * 同时将多个 field-value (键-值)对设置到哈希表key中
     *
     * @param key      缓存key
     * @param valueMap 键值对
     * @return
     */
    @Override
    public String hmset(String key, Map<String, String> valueMap) {
        String result = "false";
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hmset(key, valueMap);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in hmset", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao hmset key:" + key + " result:" + result);
        return result;
    }

    /**
     * 获取所有给定字段的值
     *
     * @param key    缓存key
     * @param fields 字段列表
     * @return
     */
    @Override
    public List<String> hmget(String key, String... fields) {
        List<String> result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hmget(key, fields);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in hmget", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao hmget key:" + key + " result:" + result);
        return result;
    }

    /**
     * 删除一个或多个哈希表字段
     *
     * @param key    缓存key
     * @param fields 字段列表
     * @return
     */
    @Override
    public Long hdel(String key, String... fields) {
        Long result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hdel(key, fields);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in hdel", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao hdel key:" + key + " result:" + result);
        return result;
    }

    /**
     * 为哈希表 key 中的指定字段的整数值加上增量 value
     *
     * @param key   缓存key
     * @param field 字段
     * @param value 增量
     * @return
     */
    @Override
    public Long hincrBy(String key, String field, long value) {
        Long result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hincrBy(key, field, value);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in hincrBy", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao hincrBy key:" + key + " result:" + result);
        return result;
    }

    /**
     * 为哈希表 key 中的指定字段的浮点数值加上增量 value
     *
     * @param key   缓存key
     * @param field 字段
     * @param value 增量
     * @return
     */
    @Override
    public Double hincrByFloat(String key, String field, double value) {
        Double result = null;
        if (ConverterUtil.isEmpty(key)) {
            return result;
        }
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedis.hincrByFloat(key, field, value);
        } catch (Exception e) {
            // 修正 Use a logger to log this exception.
            logger.error("Exception in hincrByFloat", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        logger.debug("JedisDao hincrByFloat key:" + key + " result:" + result);
        return result;
    }

    /**
     * 对key的值做减减操作,如果key不存在,则设置key为-1
     *
     * @param key
     * @return
     */
    @Override
    public Long decr(String key) {
        Jedis jedis = getResource();
        return jedis.decr(key);
    }

    /**
     * 减去指定的值
     *
     * @param key
     * @param integer
     * @return
     */
    @Override
    public Long decrBy(String key, long integer) {
        Jedis jedis = getResource();
        return jedis.decrBy(key, integer);
    }

    /**
     * Read redis to List
     *
     * @param key
     * @param clazz
     * @param <T>
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> getArrayBean(final String key, Class<T> clazz) {
        return getSerialize(key, List.class);
    }
}
