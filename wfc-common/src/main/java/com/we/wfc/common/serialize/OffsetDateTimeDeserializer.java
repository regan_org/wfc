package com.we.wfc.common.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * @Description: OffsetDateTime反序列化
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/20 11:33 上午
 */
public class OffsetDateTimeDeserializer extends JsonDeserializer<OffsetDateTime> {
    public static final OffsetDateTimeDeserializer INSTANCE = new OffsetDateTimeDeserializer();

    @Override
    public OffsetDateTime deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException, JsonProcessingException {
        return getDeserialized(arg0.getText());
    }

    public OffsetDateTime getDeserialized(String str) {
        if (null != str && !"".equals(str)) {
            if (str.contains(":")) {
                LocalDateTime loc = LocalDateTime.parse(str, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                return OffsetDateTime.of(loc, ZoneOffset.of("+08:00"));
            }
            LocalDate date2 = LocalDate.parse(str, DateTimeFormatter.ISO_DATE);
            return OffsetDateTime.of(date2, LocalTime.of(0, 0, 0), ZoneOffset.of("+08:00"));
        } else {
            return null;
        }
    }
}
