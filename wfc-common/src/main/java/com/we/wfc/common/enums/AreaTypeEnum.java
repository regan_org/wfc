package com.we.wfc.common.enums;

import com.we.wfc.common.base.BaseEnum;

/**
 * 行政区划类型 枚举
 *
 * @author zhangby
 * @date 4/1/20 4:45 pm
 */
public enum AreaTypeEnum implements BaseEnum {
    /**
     * 国家
     */
    COUNTRY("国家", "1"),
    /**
     * 省份
     */
    PROVINCE("省份", "2"),
    /**
     * 市级
     */
    CITY("市级", "3"),
    /**
     * 县级
     */
    COUNTY("县级", "4"),
    /**
     * 街道
     */
    STREET("街道", "5"),
    /**
     * 居委会
     */
    NEIGHBORHOOD("居委会", "6"),
    /**
     * 其他
     */
    OTHER("其他", "7"),
    ;

    private String label;
    private String value;

    AreaTypeEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
