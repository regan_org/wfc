package com.we.wfc.common.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description: Jedis接口
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:34 上午
 */
public interface JedisDao {
    /**
     * 默认超时时间(毫秒)
     */
    final static int DEFALUT_TIMEOUT = 10 * 1000;

    /**
     * 日志对象
     */
    Logger logger = LoggerFactory.getLogger(JedisDao.class);

    /**
     * 取得字符串Redis数据
     *
     * @param key 键
     * @return
     */
    String getStr(String key);

    /**
     * 设置字符串Redis数据
     *
     * @param key   键
     * @param value 值
     * @return
     */
    boolean setStr(String key, String value);

    /**
     * 设置可过期的字符串Redis数据
     *
     * @param key     键
     * @param value   值
     * @param seconds 过期时间(秒)
     * @return
     */
    boolean setStrExpire(String key, String value, int seconds);

    /**
     * 删除字符串Redis数据
     *
     * @param keys 键
     * @return
     */
    Long delStr(String... keys);

    /**
     * 取得序列化Redis数据
     *
     * @param key   键
     * @param clazz 实体类型
     * @return
     */
    <T> T getSerialize(String key, Class<T> clazz);

    /**
     * 设置序列表Redis数据
     *
     * @param key 键
     * @param obj 实体
     * @return
     */
    boolean setSerialize(String key, Object obj);

    /**
     * 设置可过期的字符串Redis数据
     *
     * @param key     键
     * @param value   值
     * @param seconds 过期时间(秒)
     * @return
     */
    boolean setSerializeExpire(String key, Object value, int seconds);

    /**
     * 删除序列化Redis数据
     *
     * @param keys
     * @return
     */
    Long delSerialize(String... keys);

    /**
     * 取得Redis中的字符串key
     *
     * @param patten 表达式
     * @return
     */
    Set<String> getKeys(String patten);

    /**
     * 取得Redis中的比特key
     *
     * @param patten 表达式
     * @return
     */
    Set<byte[]> getByteKeys(String patten);

    /**
     * 取得Redis中某字符串key的过期时间
     *
     * @param key
     * @return
     */
    long getExpire(String key);

    /**
     * 设置Redis中某字符串key的过期时间
     *
     * @param key     缓存key
     * @param seconds 过期时间 单位：秒
     * @return
     */
    long setExpire(String key, int seconds);

    /**
     * 判断指定key是否存在
     *
     * @param key
     * @return
     */
    Boolean exists(String key);

    /**
     * 将 key 中储存的数字值增一
     *
     * @param key
     * @return
     */
    Long incr(String key);

    /**
     * 同时将多个 field-value (键-值)对设置到哈希表key中
     *
     * @param key      缓存key
     * @param valueMap 键值对
     * @return
     */
    String hmset(String key, Map<String, String> valueMap);

    /**
     * 获取所有给定字段的值
     *
     * @param key    缓存key
     * @param fields 字段列表
     * @return
     */
    List<String> hmget(String key, String... fields);

    /**
     * 删除一个或多个哈希表字段
     *
     * @param key    缓存key
     * @param fields 字段列表
     * @return
     */
    Long hdel(String key, String... fields);

    /**
     * 为哈希表 key 中的指定字段的整数值加上增量 value
     *
     * @param key   缓存key
     * @param field 字段
     * @param value 增量
     * @return
     */
    Long hincrBy(String key, String field, long value);

    /**
     * 为哈希表 key 中的指定字段的浮点数值加上增量 value
     *
     * @param key   缓存key
     * @param field 字段
     * @param value 增量
     * @return
     */
    Double hincrByFloat(String key, String field, double value);

    /**
     * 对key的值做减减操作,如果key不存在,则设置key为-1
     *
     * @param key
     * @return
     */
    Long decr(String key);

    /**
     * 减去指定的值
     *
     * @param key
     * @param integer
     * @return
     */
    Long decrBy(String key, long integer);

    /**
     * 读取缓存中的数组
     *
     * @param key
     * @param clazz
     * @param <T>
     * @param <T>
     * @return
     */
    <T> List<T> getArrayBean(final String key, Class<T> clazz);
}
