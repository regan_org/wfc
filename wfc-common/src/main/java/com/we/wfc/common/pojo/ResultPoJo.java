package com.we.wfc.common.pojo;

import com.we.wfc.common.enums.ReturnCode;
import com.we.wfc.common.exception.WeReturn;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @Description: 自定义返回Pojo工具类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/31 5:17 下午
 */
@Setter
@Getter
@Accessors(chain = true)
public class ResultPoJo<T> {

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回消息
     */
    private String msg;

    /**
     * 返回实体
     */
    private T result;

    public ResultPoJo(T result) {
        this.result = result;
    }

    public ResultPoJo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultPoJo() {
        this.code = ReturnCode.SUCCESS.getCode();
        this.msg = ReturnCode.SUCCESS.getMsg();
    }

    public static ResultPoJo create() {
        return new ResultPoJo();
    }

    public static ResultPoJo ok(Object s) {
        return ResultPoJo.create().setResult(s);
    }

    public static ResultPoJo ok() {
        return ResultPoJo.create();
    }

    public static ResultPoJo error(ReturnCode returnCode){
        ResultPoJo resultPoJo = ResultPoJo.create()
                .setCode(returnCode.getCode())
                .setMsg(returnCode.getMsg());
        return resultPoJo;
    }

    /**
     * 通过枚举来设置
     *
     * @param errorStatus
     */
    public void setErrorStatus(WeReturn errorStatus) {
        this.code = errorStatus.getCode();
        this.msg = errorStatus.getMsg();
    }
}
