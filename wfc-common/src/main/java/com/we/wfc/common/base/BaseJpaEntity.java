package com.we.wfc.common.base;

import cn.hutool.core.lang.Dict;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.we.wfc.common.serialize.OffsetDateTimeDeserializer;
import com.we.wfc.common.serialize.OffsetDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Map;

/**
 * @author wangjiahao
 * @version 1.0
 * @since 2019/10/22 5:06 下午
 */
@Setter
@Getter
@MappedSuperclass
@Accessors(chain = true)
public abstract class BaseJpaEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "weid")
    @GenericGenerator(name = "weid", strategy = "uuid")
    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    private String id;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @ApiModelProperty(hidden = true)
    public OffsetDateTime createDate;

    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    public String createBy;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @ApiModelProperty(hidden = true)
    public OffsetDateTime updateDate;

    @Column(length = 100)
    @ApiModelProperty(hidden = true)
    public String updateBy;

    @Column(nullable = false, length = 1)
    @ApiModelProperty(hidden = true)
    private Byte delFlag = 0;

    /**
     * 多余属性
     */
    @Transient
    @JsonIgnore
    private Dict extMap = Dict.create();

    /**
     * 根据key获取值
     * @param key
     * @return
     */
    public Object get(String key) {
        return extMap.get(key);
    }

    /**
     * 设置值
     * @param key
     * @param val
     */
    public void set(String key, Object val) {
        extMap.set(key, val);
    }


    @ApiModelProperty(hidden = true)
    public Map<String, Object> getTails() {
        return extMap;
    }
}
