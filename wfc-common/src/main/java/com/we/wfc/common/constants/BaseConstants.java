package com.we.wfc.common.constants;

/**
 * @Description: 基本常量类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/10/30 4:32 下午
 */
public class BaseConstants {

    /**
     * Swagger UI api 校验策略ID
     */
    public final static String SWAGGER_AUTHORIZATION_ID = "1";

    /** 一般常量-start */
    /**
     * 是
     */
    public final static String YES = "1";
    /**
     * 否
     */
    public final static String NO = "0";
    /** 一般常量-end */

    /**
     * 用户基本属性-start
     */
    public final static String MALE = "1";
    public final static String FEMALE = "0";
    /** 用户基本属性-end */

    /** 用户类型-start */
    /**
     * 当前登录用户key
     */
    public final static String CURRENT_USER_ID = "current_user_id";
    /**
     * 系统管理员ID
     */
    public final static String USER_ADMIN_ID = "1";
    /**
     * 管理员角色ID
     */
    public final static String ROLE_ADMIN_ID = "1";
    /**
     * 默认父类ID
     */
    public final static String SYS_PARENT_ID = "0";
    /** 用户类型-end */

    /**
     * 角色类型-start
     */
    public final static String USER_TYPE_USER = "001";
    public final static String USER_TYPE_ADMIN = "002";
    public final static String USER_TYPE_SELLER = "003";
    public final static String USER_TYPE_CAR_OWNER = "004";
    /** 角色类型-end */

    /**
     * 用户状态-start
     */
    public final static int NORMAL_USER = 0;
    public final static int LOCK_USER = 1;
    /** 用户状态-end */

    /**
     * 短信邮箱-start
     */
    public final static String TITLE_TEXT_VAILDCODE = "逸修登陆验证码";
    /** 短信邮箱-end */

    /** 文件-start */
    /**
     * 类型：0 图片
     */
    public static final String TYPE_IMAGE = "0";
    /**
     * 类型：1 文件
     */
    public static final String TYPE_FILE = "1";
    /**
     * 类型：2 音视频
     */
    public static final String TYPE_VIDEO = "2";
    /** 文件-end */

    /** security jwt key start */
    /**
     * jwt key
     */
    public static final String JWT_KEY = "wfc-security-jwt";
    /**security jwt key end  */
}
