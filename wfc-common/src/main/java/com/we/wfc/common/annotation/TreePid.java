package com.we.wfc.common.annotation;

import java.lang.annotation.*;

/**
 * @Description: 树父id
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/12/30 2:25 下午
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TreePid {
    
}
