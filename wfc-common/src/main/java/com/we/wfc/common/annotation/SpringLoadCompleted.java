package com.we.wfc.common.annotation;

import org.springframework.context.ApplicationContext;

/**
 * @Description: LoadCompleted接口规范
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
public interface SpringLoadCompleted {

    /**
     * 初始化(必须实现)
     */
    void init(ApplicationContext context);
}
