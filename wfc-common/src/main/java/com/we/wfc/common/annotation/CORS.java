package com.we.wfc.common.annotation;

import com.we.wfc.common.config.CorsAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @Description: 跨域注解
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019/11/1 9:30 上午
 */
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE})
@Documented
@Import(CorsAutoConfiguration.class)
@Configuration
public @interface CORS {
    /**
     * 跨域源
     */
    String origin() default "*";

    /**
     * 头信息
     */
    String header() default "*";

    /**
     * 方法
     */
    String method() default "*";

    /**
     * 允许跨域的URL
     */
    String allowUrl() default "/**";
}