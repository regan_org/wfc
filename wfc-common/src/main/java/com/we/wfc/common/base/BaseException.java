package com.we.wfc.common.base;

import com.we.wfc.common.enums.ReturnCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 定义基础异常类
 * @Author:Liangzy(Feeling)
 * @Date:Create in 2019-08-05 11:41
 */
@Setter
@Getter
public class BaseException extends RuntimeException {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = 5580471195124522987L;

    /**
     * 错误码
     */
    protected String errorCode;

    /**
     * 错误消息
     */
    protected String errorMessage;

    /**
     * 异常
     */
    protected Exception exp;

    public BaseException() {

    }

    public BaseException(String errorCode, String errorMessage) {
        super("error code:" + errorCode + " error message:" + errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BaseException(ReturnCode resultInfo, Exception exception) {
        super("error code:" + resultInfo.getCode() + " error message:" + resultInfo.getMsg());
        this.errorCode = resultInfo.getCode();
        this.errorMessage = resultInfo.getMsg();
        this.exp = exception;
    }

    public BaseException(ReturnCode resultInfo) {
        super("error code:" + resultInfo.getCode() + " error message:" + resultInfo.getMsg());
        this.errorCode = resultInfo.getCode();
        this.errorMessage = resultInfo.getMsg();
    }

    public BaseException(String code) {
        this.errorCode = code;
    }
}
